# Air Comparison Pyconmeter

An air comparison pycnometer is incredibly easy to use: 

- You put your sample into the measurement piston, 
- you decrease the volumina of measurement piston and comparison piston ensuring equal pressure in
  both of them until the stop position of the comparison piston is
  reached, 
- you read the volume of your sample directly from the scale at the measurement piston.

But the working principle of the air comparison pycnometer is not so
easy to grasp. The aim of this video is to help students understand
how the [general gas equation](https://en.wikipedia.org/wiki/Ideal_gas_law) is used to
determine the [true volume]() of the solid phase in a dispersed system *solid in gas*.
