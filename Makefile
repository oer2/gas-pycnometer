MANIM=manim
QUALITY=qm

# Outline
outl1: pycnometer.py
	$(MANIM) $< OutlineScene1 -p$(QUALITY)

outl2: pycnometer.py
	$(MANIM) $< OutlineScene2 -p$(QUALITY)

outl3: pycnometer.py
	$(MANIM) $< OutlineScene3 -p$(QUALITY)

# cylinder scene
cyl: pycnometer.py
	$(MANIM) $< CylinderScene -p$(QUALITY)

# empty pycnometer scene
empty: pycnometer.py
	$(MANIM) $< EmptyPycnometerScene -p$(QUALITY)

# qualitative pycnomeer scene
ql: pycnometer.py
	$(MANIM) $< QualitativePycnometerScene -p$(QUALITY)

# qualitative pycnometer scene
qn: pycnometer.py
	$(MANIM) $< QuantitativePycnometerScene -p$(QUALITY)

movie:
	cd media/videos/pycnometer/720p30
	mkvmerge -o pyknometer.mkv OutlineScene1.mp4 EmptyPycnometerScene.mp4 OutlineScene2.mp4 QualitativePycnometerScene.mp4 OutlineScene3.mp4 QuantitativePycnometerScene.mp4
