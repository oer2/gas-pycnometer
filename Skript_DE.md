# Einleitung

Mit dem Luftvergleichspyknometer kann das Volumen der festen Phase eines Haufwerks bestimmt werden.

Es besteht aus zwei identischen zylindrischen Behältern, die über ein Differendruckmanometer (sic) verbunden sind.

Mittels zweier beweglicher Kolben kann das Volumen in beiden Zylindern verändert werden.



# Funktionsweise


# Auswertung



## alternative Überlegung

Könnte man alles Gasvolumen in beiden Kammern auf null verdichten ($\lim_{p\to\infty} V$), gälte $V_s = \Delta V$.
