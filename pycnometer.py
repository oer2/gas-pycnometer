#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from manim import *

import operator as op

from functools import reduce
from random import random

# THK colors
THK_RED     = '#c81e0f'
THK_ORANGE  = '#ea5a00'
THK_MAGENTA = '#b43092'

# Objects
class Particle(Rectangle):

    def __init__(self, size, pos, xshift=1, color=BLUE, **kwargs):
        super().__init__(color, size, size, stroke_width=0, **kwargs)

        # target
        i, j = pos              # position in grid after rearrangement
        self.generate_target()
        self.target.shift(0.5*size**(RIGHT + UP)) # align at br
        self.target.shift(UP*size*i)
        self.target.shift(RIGHT*size*j)
        self.target.set_opacity(1)

        # initial position
        r, θ = 0.4*random(), 2*PI*random() # polar coordinates
        φ = PI*random()                    # rotation of particle
        self.shift(r*np.cos(θ)*RIGHT + r*np.sin(θ)*UP)
        self.shift(xshift*RIGHT + 0.7*RIGHT - size*RIGHT + 0.5* UP)
        self.rotate(φ)


class Cylinder(VMobject):

    CONFIG = {
        "cylinder_width"   : 2,
        "cylinder_height"  : 1,
        "piston_width"     : 0.2,
        "particle_size"    : 0.02,
        "particle_color"   : BLUE,
        "particle_opacity" : 0.9,
        "particle_grid"    : (25, 4), # after rearrangement
    }

    def __init__(self, show_connectors=False, **kwargs):
        height = self.CONFIG["cylinder_height"]
        cwidth = self.CONFIG["cylinder_width"]
        pwidth = self.CONFIG["piston_width"]

        # piston
        p0 = Rectangle(GRAY, height, pwidth, stroke_width=0, fill_opacity=1)
        p0.shift(0.5*p0.height*UP)
        p0.shift(0.5*p0.width*LEFT)

        p1 = Rectangle(GRAY, 0.2*height, 0.5*height, stroke_width=0, fill_opacity=1)
        p1.shift(0.5*p0.height*UP)
        p1.shift((p0.width + 0.5*p1.width)*LEFT)
        self.piston = VDict([("p0", p0), ("p1", p1)])

        # wall (around piston)
        lwc = config.frame_height/config.pixel_height*DEFAULT_STROKE_WIDTH
        right = (cwidth + 2.5*lwc)*RIGHT
        w1 = Line(p0.width*LEFT, right).shift(p0.height*UP)
        w2 = Line(p0.width*LEFT, right)
        # linewidth correction
        w1.shift(0.5*lwc*UP)
        w2.shift(0.5*lwc*DOWN)

        # connectors
        c0 = (w1.get_end() + w2.get_end())/2
        c1 = c0 + 0.1*height*UP
        c2 = c0 + 0.1*height*DOWN
        w3 = Line(w1.get_end() + 0.5*lwc*UP, c1 + 0.5*lwc*DOWN)
        w4 = Line(w2.get_end() + 0.5*lwc*DOWN, c2 + 0.5*lwc*UP)
        w5 = Line(w3.get_end() + lwc*LEFT, w3.get_end() + 5.5*lwc*RIGHT)
        w6 = Line(w4.get_end() + lwc*LEFT, w4.get_end() + 5.5*lwc*RIGHT)

        # particles
        k, l = self.CONFIG['particle_grid']
        particles = VGroup(*[ Particle(size=p0.height/k, pos=(i, j), 
                                       color=self.CONFIG['particle_color'])
                              for i in range(k) for j in range(l) ])

        # create mobject
        super().__init__(**kwargs)

        # particle block (to be attached to piston)
        self.generate_particle_block(0)

        self.wall = VGroup(w1, w2, w3, w4)
        if show_connectors:
            self.wall = VGroup(w5, w6)
        self.connector = c1, c2
        self.particles = particles

        self.add(self.piston, self.wall, self.particles)

        self.X0 = self.piston.get_center()
        self.set_position(0)

    def shift(self, *vectors):
        super().shift(*vectors)
        total_vector = reduce(op.add, vectors)
        for p in [self.X0, *self.connector]:
            p += total_vector
        for p in self.particles:
            p.target.shift(total_vector)
        return self
        
    def show_particles(self) -> Animation:
        for p in self.particles:
            p.set_opacity(1)
        return ShowCreation(self.particles)

    def generate_particle_block(self, pos, opacity=0.0):
        p0 = self.piston['p0']
        k, l = self.CONFIG['particle_grid']
        particle_color = self.CONFIG['particle_color']
        pb_width = p0.height*l/k*(1 - 0.8*pos)

        p2 = Rectangle(particle_color, p0.height, pb_width, stroke_width=0, fill_opacity=opacity)
        p2.next_to(p0, RIGHT, buff=0)
        self.piston['p2'] = p2

    def show_particle_block(self, opacity=1):
        for p in self.particles:
            p.set_opacity(0)
        self.piston['p2'].set_opacity(opacity)

    def arrange_particles(self):
        n = len(self.particles)
        return [ MoveToTarget(p) for p in self.particles ]

    def get_X(self):
        piston_R = self.piston.get_right()
        return piston_R[0]

    def set_position(self, pos, filled=False):
        """ set piston to position in intervall 0 to 1,
            respect size of solid state if filled==True """
        if pos >= 0:
            self.position = pos # should possibly be a value tracker
            cwidth = self.CONFIG['cylinder_width']
            piston_pos = self.X0 + pos*cwidth*RIGHT
            if filled:
                solid_width = self.piston['p2'].width
                piston_pos += solid_width*pos*LEFT
            if self.piston['p2'].fill_opacity < 0.8:
                self.generate_particle_block(pos, opacity=self.piston['p2'].fill_opacity)
            self.piston.move_to(piston_pos)

    def get_particle_color(self):
        return self.CONFIG['particle_color']


class Manometer(VMobject):
    
    colors = color_gradient((GREEN, YELLOW, RED), 100)

    def __init__(self, color=GRAY, **kwargs):

        line = Line(UP, DOWN, color=LIGHT_GREY)
        line.add_tip(tip_length=0.1, at_start=True)
        line.add_tip(tip_length=0.1, at_start=False)
        label = MathTex(r"\Delta p").scale(0.5)
        tick = Line(0.05*LEFT, 0.05*RIGHT, stroke_width=0.5*DEFAULT_STROKE_WIDTH)
        mark = MathTex("0").scale(0.5)
        dot = Dot(color=self.colors[0])

        super().__init__(self, **kwargs)
        self.dp = 0             # should possibly be a value tracker
        self.line = line
        self.tick = tick
        self.label = label
        self.mark = mark
        self.dot = dot

        self.label.add_updater(lambda l: l.next_to(line, UP, buff=SMALL_BUFF))
        self.tick.add_updater(lambda t: t.move_to(line.get_center()))
        self.mark.add_updater(lambda m: m.next_to(tick, RIGHT, buff=SMALL_BUFF))

        self.add(line, tick, mark, label, dot)

    def shift(self, *args):
        for mob in self.submobjects:
            mob.shift(*args)

    def move_to(self, *args):
        self.line.move_to(*args)
        self.tick.move_to(self.line.get_center())
        self.mark.next_to(self.tick, RIGHT, buff=SMALL_BUFF)
        self.label.next_to(self.line, UP, buff=SMALL_BUFF)

    def set_dp(self, dp):
        color_val = (len(self.colors) - 1)*dp
        color_ind = int(abs(color_val))
        dot_position = self.line.get_center() + 0.7*dp*UP

        self.dp = dp
        self.dot.move_to(dot_position)
        self.dot.set_color(self.colors[color_ind])
        

class Connector(VMobject):
    """ connect two Cylinders with difference manometer """

    def __init__(self, cyl1, cyl2, delta_p=0, length=1, **kwargs):
        """ manometer indicates an overpressure in the lower cylinder, -1 <= delta_p <= 1 """

        lwc = config.frame_height/config.pixel_height*DEFAULT_STROKE_WIDTH
        o1, o2 = cyl1.connector

        a1 = o1 + (length + np.linalg.norm(o2 - o1) + lwc)*RIGHT
        b1 = o2 + (length - lwc)*RIGHT
        
        p1, p2 = cyl2.connector
        c1 = p1 + (length - lwc)*RIGHT
        d1 = p2 + (length + np.linalg.norm(p2 - p1) + lwc)*RIGHT

        center = (a1 + b1 + c1 + d1)/4
        e1 = (b1 + c1)/2
        f1 = (a1 + d1)/2
        
        m1 = Arc(PI/2,   PI, radius=0.3).shift(e1)
        m2 = Arc(PI*3/2, PI, radius=0.3).shift(f1)
        
        a2 = m2.points[-1]
        d2 = m2.points[0]
        b2 = m1.points[0]
        c2 = m1.points[-1]

        lines = [ Line(o1, a1), Line(o2, b1), 
                  Line(p1, c1), Line(p2, d1),
                  Line(a1, a2), Line(d1, d2),
                  Line(b1, b2), Line(c1, c2)
        ]

        super().__init__(self, **kwargs)
        self.ma = (m2.points[15] + m2.points[16])/2
        self.mb = (m1.points[15] + m1.points[16])/2

        self.m3 = Line(self.ma, self.mb, color=RED)
        self.m4 = Manometer()
        self.m4.line.next_to(self.m3, RIGHT, buff=MED_LARGE_BUFF)

        self.add(VGroup(*lines))
        self.add(m1, m2)
        self.add(self.m3)
        self.add(self.m4)
        self.set_delta_p(delta_p)

    def set_delta_p(self, delta_p):
        self.set_membrane(delta_p)

    def set_membrane(self, delta_p):
        self.m3.set_path_arc(0.5*PI*delta_p)
        self.m4.set_dp(delta_p)


# Scenes

class OutlineScene(Scene):

    def setup(self) -> None:
        headings = [ Tex("Air Comparison Pycnometer").scale(1.5),
                     Tex(r"Construction"),
                     Tex(r"Qualitative functioning"),
                     Tex(r"Quantitative functioning")
        ]

        title = headings[0]
        title.to_edge(UP)
        for i, h in enumerate(headings[1:]):
            h.next_to(4*LEFT + UP + i*DOWN, RIGHT, buff=0)

        self.headings = headings

    def construct(self) -> None:

        self.play(Write(self.headings[0]))
        self.wait(2)

    def indicate_heading(self, heading, color=THK_MAGENTA) -> Animation:
        return heading.animate.set_color(color)

    def show_outline(self) -> Animation:
        return ( FadeIn(h) for h in self.headings )

    def make_outline(self, write=False):
        self.bullets = []
        for h in self.headings[1:]:
            bullet = Dot()
            bullet.next_to(h, LEFT, buff=MED_LARGE_BUFF)
            self.bullets.append(bullet)
            if write:
                self.play(FadeIn(bullet), run_time=0.5)
                self.play(Write(h))
                self.wait()
        if not write:
            self.play( *[FadeIn(h) for h in self.headings[1:] ],
                       *[FadeIn(b) for b in self.bullets ],
            )


    def make_title(self, index) -> None:
        heading = self.headings.pop(index)
        self.play(self.indicate_heading(heading), run_time=2)
        self.wait(1)

        heading.generate_target(use_deepcopy=False)
        heading.target.move_to(self.headings[0])
        heading.target.set_color(WHITE)
        self.play(MoveToTarget(heading),
                  *[ FadeOut(b) for b in self.bullets ],
                  *[ FadeOut(h) for h in self.headings ]
        )
        self.wait(2)

    def set_title(self, index):
        title = self.headings[index]
        title.move_to(self.headings[0])
        self.add(title)
        self.wait()
        return title

class OutlineScene1(OutlineScene):

    def construct(self) -> None:
        super().construct()
        self.make_outline(write=True)
        self.wait()
        self.make_title(1)


class OutlineScene2(OutlineScene):

    def construct(self) -> None:
        self.play(FadeIn(self.headings[0]))
        self.make_outline(write=False)
        self.wait()
        self.make_title(2)


class OutlineScene3(OutlineScene):

    def construct(self) -> None:
        self.play(FadeIn(self.headings[0]))
        self.make_outline(write=False)
        self.wait()
        self.make_title(3)


class CylinderScene(Scene):

    def setup(self) -> None:
        p = ValueTracker(0)
        c1 = Cylinder()
        c1.add_updater(lambda x: c1.set_position(p.get_value()))

        self.p = p
        self.c1 = c1

    def construct(self) -> None:
        p, c1 = self.p, self.c1

        self.play(FadeIn(c1))
        self.play(p.animate.set_value(1), run_time=2)
        self.play(p.animate.set_value(0), run_time=2)
        self.wait()
        self.play(p.animate.set_value(1), run_time=2)
        self.wait(2)

        
class EmptyPycnometerScene(OutlineScene):

    def setup(self):
        super().setup()
        self.c1 = Cylinder().shift(2*LEFT + 0.5*UP)
        self.c2 = Cylinder().shift(2*LEFT + 2.5*DOWN)
        self.c3 = Connector(self.c1, self.c2)

    def construct(self):
        title = self.set_title(1)
        c1, c2, c3 = self.c1, self.c2, self.c3
        
        p = ValueTracker(0)
        c1.add_updater(lambda x: x.set_position(-1*p.get_value()))
        c2.add_updater(lambda x: x.set_position(p.get_value()))
        c3.add_updater(lambda x: x.set_delta_p(p.get_value()))

        self.play(FadeIn(c1), FadeIn(c2), FadeIn(c3))

        self.play(Indicate(c1), color=THK_ORANGE, run_time=2)
        self.play(Indicate(c2), color=THK_ORANGE, run_time=2)
        self.wait()
        self.play(Indicate(c3), color=THK_ORANGE, run_time=2)
        self.wait(2)

        self.play(p.animate.set_value(-0.8), run_time=2)
        self.play(p.animate.set_value(0), run_time=2)
        self.play(p.animate.set_value(0.8), run_time=2)
        self.play(p.animate.set_value(0), run_time=2)
        self.wait()

        c1.clear_updaters()
        c2.clear_updaters()
        c3.clear_updaters()
        self.play(
            c1.animate.set_position(0.5),
            c2.animate.set_position(0.5),
            run_time=2
        )
        self.play(
            c1.animate.set_position(0),
            c2.animate.set_position(0),
            run_time=2
        )
        self.wait(3)
        self.play(FadeOut(title), FadeOut(c1), FadeOut(c2), FadeOut(c3))

        
class QualitativePycnometerScene(EmptyPycnometerScene):

    def dv_indicator(self):
        indicator = VGroup()
        p1 = self.c1.piston['p0'].get_corner(DR)
        p2 = self.c2.piston['p0'].get_corner(UR)

        q1 = np.array([p2[0], p1[1], 0])
        q2 = np.array([p1[0], p2[1], 0])
        
        y = np.mean([p1, p2], axis=0)

        helper_1 = DashedLine(p1, q2, stroke_width=1)
        helper_2 = DashedLine(p2, q1, stroke_width=1)
        indicator.add(helper_1, helper_2)
        
        a1 = np.array([p1[0], y[1], 0])
        a2 = np.array([p2[0], y[1], 0])
        arrow = DoubleArrow(a1, a2, stroke_width=3, buff=0, tip_length=0.08)
        arrow.set_color(YELLOW)
        indicator.add(arrow)

        label = MathTex(r'\Delta V', color=YELLOW).scale(min(0.8*arrow.get_length(), 0.5))
        label.next_to(arrow, UP, buff=SMALL_BUFF)
        indicator.add(label)

        return indicator

    def construct(self):
        c1, c2, c3 = self.c1, self.c2, self.c3
        title = self.set_title(2)

        self.play(FadeIn(c1), FadeIn(c2), FadeIn(c3))
        self.play(c2.show_particles())

        # building up a pressure difference
        self.play(
            c1.animate.set_position(0.4),
            c2.animate.set_position(0.4),
            c3.animate.set_membrane(0.5),
            run_time=2
        )
        self.wait(2)

        # removing the pressure difference
        dV = self.dv_indicator()
        dV.add_updater(lambda v: v.become(self.dv_indicator()))
        self.play(FadeIn(dV), run_time=2)

        self.play(
            c2.animate.set_position(0.25),
            c3.animate.set_membrane(0.0),
            run_time=4
        )
        
        self.wait()
        self.play(
            c1.animate.set_position(0.8),
            c2.animate.set_position(0.50),
            run_time=6
        )
        self.wait(3)
        self.play(FadeOut(dV))
        self.play(FadeOut(title), FadeOut(c1), FadeOut(c2), FadeOut(c3))


class QuantitativePycnometerScene(QualitativePycnometerScene):

    def create_particles_ghost_c1(self, *args, **kwargs) -> None:
        pb = self.c2.piston['p2'].copy()
        pb.generate_target()
        pb.target.move_to(self.c1.piston['p2'])
        pb.target.set_opacity(0.5)
        self.play(MoveToTarget(pb), *args, **kwargs)
        self.c1.show_particle_block(0.5)
        self.remove(pb)

    def koeff_transform(self, denom) -> [Transform]:
        col = self.c1.get_particle_color()
        d = MathTex(str(denom)).scale(0.8)
        f = MathTex(rf'\frac{{{denom - 1}}}{{{denom}}}', color=col).scale(0.8)
        t = [ Transform(self.eqn1[3], d.copy().set_color(col).move_to(self.eqn1[3])),
              Transform(self.eqn1[8], d.copy().move_to(self.eqn1[8])),
              Transform(self.eqn2[6], d.move_to(self.eqn2[6])),
              Transform(self.eqn3[2], f.move_to(self.eqn3[2]))
        ]
        return t

    def transform_dV_equations_to_denom(self, denom) -> Animation:
        assert denom > 1, "denominator must be greater than 1"
        if denom == 2:
            t1 = [ self.eqn1[i].animate.set_opacity(1) for i in [2, 3, 4, 7, 8, 9] ]
            t2 = [ self.eqn2[i].animate.set_opacity(1) for i in [5, 6, 7] ]
            t3 = [ self.eqn3[i].animate.set_opacity(1) for i in range(2, 4) ]
            t3.append(Transform(self.eqn3[3], self.eqn3b))
            return (*t1, *t2, *t3)
        else:
            col = self.c1.get_particle_color()
            d = MathTex(str(denom)).scale(0.8)
            f = MathTex(rf'\frac{{{denom - 1}}}{{{denom}}}', color=col).scale(0.8)
            t = ( Transform(self.eqn1[3], d.copy().set_color(col).move_to(self.eqn1[3])),
                  Transform(self.eqn1[8], d.copy().move_to(self.eqn1[8])),
                  Transform(self.eqn2[6], d.move_to(self.eqn2[6])),
                  Transform(self.eqn3[2], f.move_to(self.eqn3[2]))
            )
            return t

    def setup(self) -> None:
        super().setup()
        eqn1 = MathTex('V_1', '=', r'\frac1{', '2', '}',  'V_s', '+', r'\frac1{', '2', '}', 'V_g').scale(0.8)
        eqn2 = MathTex('V_2', '=', r'\hphantom{\frac12}', 'V_s', '+', r'\frac1{', '2', '}', 'V_g').scale(0.8)
        eqn3 = MathTex(r'\Delta V_{\hphantom{2}}', '=', r'\frac{1}{2}', '\medspace 0').scale(0.8)
        eqn3b =  MathTex('V_s').scale(0.8)

        eqn1.next_to(self.c1.get_corner(UL), UR)
        eqn2.next_to(self.c2.get_corner(DL), DR)
        eqn3.next_to((eqn1[1].get_left() + eqn2[1].get_left())/2, RIGHT, buff=0, index_of_submobject_to_align=1)
        eqn3b.move_to(eqn3[3])

        col = self.c1.get_particle_color()
        for i in range(2, 6): eqn1[i].set_color(col)
        eqn2[3].set_color(col)
        eqn3[2].set_color(col)
        eqn3b.set_color(col)

        # prepare equations for position 0:
        for i in [2, 3, 4, 7, 8, 9]:  eqn1[i].set_opacity(0)
        for i in [5, 6, 7]: eqn2[i].set_opacity(0)
        eqn3[2].set_opacity(0)

        self.eqn1 = eqn1
        self.eqn2 = eqn2
        self.eqn3 = eqn3
        self.eqn3b = eqn3b

    def construct(self) -> None:
        c1, c2, c3 = self.c1, self.c2, self.c3
        eqn1, eqn2, eqn3, eqn3b = self.eqn1, self.eqn2, self.eqn3, self.eqn3b

        title = self.set_title(3)
        self.play(FadeIn(c1), FadeIn(c2), FadeIn(c3))
        self.play(c2.show_particles())
        self.play(*c2.arrange_particles(), run_time=3)
        # self.play(c2.animate.show_particle_block(), run_time=1)
        c2.show_particle_block()

        self.play(FadeIn(eqn2))
        self.wait(2)

        self.create_particles_ghost_c1(FadeIn(eqn1), run_time=1)
        self.play(FadeIn(eqn3))
        self.wait(3)

        for denom in range(2, 6):
            v = 1 - 1/denom
            t = self.transform_dV_equations_to_denom(denom)
            self.play(
                c1.animate.set_position(v, filled=False),
                c2.animate.set_position(v, filled=True),
                *t,
                run_time=3
            )
            self.wait(3)
            
        self.play(FadeOut(title),
                  FadeOut(eqn1), FadeOut(eqn2),
                  FadeOut(c1), FadeOut(c2), FadeOut(c3))

        eqn4 = MathTex(r'\Delta V_{\hphantom{2}}', '=', r'\frac{n-1}{n}\,V_s', 
                       ',\qquad', r'n \in \mathbb{R}').scale(0.8)
        eqn4.next_to(eqn3, DOWN, buff=LARGE_BUFF, index_of_submobject_to_align=1)
        eqn4[2].set_color(BLUE)

        eqn5 = MathTex('V_s', '=', r'\frac{n}{n - 1}\, \Delta V')
        eqn5.next_to(eqn4, UP, buff=1.5, index_of_submobject_to_align=1)
        for i in (0, 2): eqn5.set_color(THK_MAGENTA)

        self.play(FadeIn(eqn4))
        self.wait()
        self.play(FadeIn(eqn5), FadeOut(eqn3))
        self.wait(2)
        self.play(FadeOut(eqn4))
        self.wait(3)

class Epilogue(QuantitativePycnometerScene):

    def construct(self):
        c1, c2, c3 = self.c1, self.c2, self.c3
        eqn1, eqn2, eqn3, eqn3b = self.eqn1, self.eqn2, self.eqn3, self.eqn3b

        self.add(c1, c2, c3)
        self.add(eqn1)

        for denom in range(2, 3):
            v = 1 - 1/denom
            t = self.transform_dV_equations_to_denom(denom)
            self.play(
                #c1.animate.set_position(v, filled=False),
                #c2.animate.set_position(v, filled=True),
                *t,
                run_time=3
            )
            self.wait(3)
            
        eqn4 = MathTex(r'\Delta V_{\hphantom{2}}', '=', r'\frac{n-1}{n}\,V_s', 
                       ',\quad', r'n \in \mathbb{R}').scale(0.8)
        eqn4.move_to(eqn3).to_edge(LEFT)
        eqn4[2].set_color(BLUE)

        eqn5 = MathTex('V_s', '=', r'\frac{n}{n - 1}\, V_0')
        eqn5.next_to(eqn4, DOWN, buff=1.5, index_of_submobject_to_align=1)
        for i in (0, 2): eqn5.set_color(THK_MAGENTA)

        self.play(FadeIn(eqn4))
        self.wait()
        self.play(FadeIn(eqn5), FadeOut(eqn3))
        self.wait(2)
        self.play(FadeOut(eqn4))
        self.wait(3)
